#!/bin/sh
set -e

#
# THIS FILE MUST HAVE UNIX LINE ENDINGS, OR THE MONGO IMAGE WILL FAIL TO START.
#

echo "Creating HEA database and user account..."
mongo $MONGO_HEA_DATABASE --authenticationDatabase admin -u $MONGO_ROOT_USERNAME -p $MONGO_ROOT_PASSWORD --eval "db.createUser({user: \"$MONGO_INITDB_HEA_USERNAME\", pwd: \"$MONGO_INITDB_HEA_PASSWORD\", roles: [\"readWrite\"]});"
echo "HEA database and user account created."

#exec "$@"
