# Health Enterprise Analytics (HEA)

HEA is a research and analytics desktop in your web browser, designed for academic medical centers that strive for
integrated research, discovery, and clinical missions. Its goals are to:

* Present a simple desktop metaphor for securely interacting with data and tools for analyzing data.
* Integrate locally developed and third-party data sources and analysis tools into a consistent and easy-to-use data 
capture, retrieval, and analysis environment.
* Make electronic health record data accessible for research and analytics, and bring research and analytics findings
to the point of care.
* Support automated chart abstraction at scale (millions of patient records) using business rules, natural language 
processing, and machine learning classification techniques.
* Support development and training of machine learning algorithms for prediction and classification.

## Version history
We have just begun development!

## Runtime requirements
* Major web browsers, including Firefox, Safari, Chrome, Edge, and Internet Explorer version 10 or greater.
* Docker 19.03.5 or greater, available from https://www.docker.com/.

## Running HEA
You can run Docker on any operating system that Docker supports. After you install Docker, create a file named `.env` 
in your `hea` directory and populate it with the following:
```
MONGO_ROOT_USERNAME=<your_username> (required)
MONGO_ROOT_PASSWORD=<your password> (required)
MONGO_HEA_DATABASE=<your_database> (will default to hea if omitted)
MONGO_HEA_USERNAME=<your username> (required)
MONGO_HEA_PASSWORD=<your password> (required)
MONGO_PORT=<the port that MongoDB listens on -- if omitted, MongoDB listens on port 27017>
HEASERVER_API_URL=<url_to_the_api> # (will default to https://localhost:8443/api if omitted)
REVERSEPROXY_PORT=<port> # (an unencrypted port; will default to port 8080 if omitted)
REVERSEPROXY_SSL_PORT=<port> # (a ssl port; will default to port 8443 if omitted)
HEASERVER_API_CRYPTO_PASSPHRASE=<passphrase> (requierd)
INDEX_SERVER_URL=<URL of the python index server that contains the HEA software packages> (required)
INDEX_SERVER_HOSTNAME=<the hostname of the python index server> (required)
FUSEKI_ADMIN_PASSWORD=<the password for the admin user of the Apache Jen Fuseki instance> (required)
FUSEKI_PORT=<the port that Apache Jena Fuseki listens on -- if omitted, Fuseki listens on port 3030>
HEASERVER_REGISTRY_VERSION=<the version of heaserver-registry to run>
HEASERVER_DATA_ADAPTERS_VERSION=<the version of heaserver-data-adapters to run>
HEASERVER_FOLDERS_VERSION=<the version of heaserver-folders to run>
```

Next, in the `environment.ts` in the `hea/heawebclient/src/directories` directory, add the following to it:
```
{
  ...
  "clientId": "<the client id from your OpenID Connect identity provider>",
  "domain": "<the domain of your OpenID Connect identity provider>"
}
```

Do the same with any other environment files that you have created.

Then, from the hea directory in your terminal, run `docker-compose build` and then
`docker-compose up`. Go to `http://localhost:<REVERSEPROXY_PORT>` in your web browser, inserting the value of the
`REVERSEPROXY_PORT` variable that you added to `.env`. To stop HEA, press Control-C.

The first time you run HEA, after `docker-compose up` is finished, run the run-first-time.sh script (run-first-time.bat 
on Windows) to initialize HEA's MongoDB database.

There will be a MongoDB instance listening on port 27017 or the port you specified in your `.env` file.

There will be an Apache Jena Fuseki instance listening on port 3030 or the port that you specified in your `.env` file.
Fuseki has a default admin account with the password you specified in your `.env` file, and you can go to
`http://localhost:3030` to access your Fuseki instance (substitute 3030 with the correct port).

### Running HEA on Windows

Note that recent versions of Docker Desktop combined
with recent versions of Windows 10 will not start Docker. Instead, you'll get an Access Denied error with a message to
add yourself to the `docker-users` group. Fixing this on Windows 10 Home Edition requires editing the registry, and we
strongly recommend using another edition of Windows. On other editions of Windows, you can fix the problem with the 
Computer Management application. As an administrator, open Computer Management, right-click on the docker-users group, 
and type in your domain\username. You will have to enter the user's password. Then restart and log back in as yourself.

The first time the mongo container starts, Windows 10 may ask you to confirm that you want to share your filesystem for 
the mongo database. It may then prompt you for a username and password. The prompt dialog may be hidden behind other 
windows, and mongo will appear to fail to start.

### Project structure
The various subdirectories contain Docker configuration files for building docker images for the various components of
HEA. It also contains the source code for the the HEA web client. The components of HEA are:
* `heaclient`: Python HTTP client library for calling HEA REST API services.
* `heaobject`: Python objects that HEA clients POST, PUT, GET, and DELETE to/from HEA REST services.
* `heaserver`: Python HEA REST API service library.
* `heawebclient`: React web client for HEA.
* `heaserver-registry`: Python microservice for managing mappings of HEA Object types to the microservices for storing 
and retrieving them.
* `heaserver-folders`: Python microservice for managing the folders of the HEA desktop.
* `heaserver-data-adapters`: Python microservice for accessing data files and databases.

## Developing the heawebclient

The heawebclient is an Angular CLI project. It comes with a `ngcli` configuration for testing using `ng serve`.
Run `ng serve --configuration=ngcli`, or simply run `npm start`.

The server side just requires one configuration change to work with Angular CLI: set the following environment
variable in your `.env`: `HEASERVER_API_URL=http://localhost:4200`. Then start the server side with
`docker-compose` as described above.

## Developing python microservices

* Python version: look at the project README.md for the version that is supported.
* Pip configuration: you will need a custom index server such as devpi to upload HEA component releases so that HEA
components can depend on each other. You will need to configure pip to use the custom index server
instead of the usual Pypi. See https://pip.pypa.io/en/stable/user_guide for details.

## Coding standards

### Style guide
* For Python code, follow the PEP-8 style guide, available from https://www.python.org/dev/peps/pep-0008/, and use
spaces for indentation, 4 spaces per indentation level.

### Versioning
Use semantic versioning as described in 
https://packaging.python.org/guides/distributing-packages-using-setuptools/#choosing-a-versioning-scheme. In addition,
while development is underway, the version should be the next version number suffixed by -SNAPSHOT.

### Uploading releases to a Python index server
These instructions assume separate stable and staging repositories. Numbered releases, including alphas and betas, go 
into the stable repository. Snapshots of works in progress go into the staging repository. Artifacts uploaded to the
staging repository can be overwritten. Artifacts uploaded to stable cannot. Thus, also use staging to upload numbered
releases, verify the uploaded packages, and then upload to stable.

From the project's root directory:
1. For numbered releases, remove -SNAPSHOT from the version number in setup.py, tag it in git to indicate a release, 
and commit to version control. Skip this step for SNAPSHOT releases. Make the commit message `Version x.x.x`, replacing
`x.x.x` with the actual version number being released.
2. Run `python setup.py sdist bdist_wheel` to create the artifacts.
3. Run `twine upload -r <repository> dist/*` to upload to the repository. The repository name has to be defined in a
twine configuration file such as `$HOME/.pypirc`.
4. For numbered releases, increment the version number in setup.py, append -SNAPSHOT to it, and commit to version 
control. Make the commit message `Next development iteration`. Skip this step for SNAPSHOT releases.
   
### Updating the version of a python microservice
Run freeze-requirements.bat (Windows) or freeze-requirements.sh (Mac or Linux) with two arguments: the microservice name
and the version number, for example, `freeze-requirements heaserver-folders 1.0.0a17`. Your .env will get updated with
the specified version number, and a new `requirements.txt` file will be generated to freeze dependencies. For this to 
work, you will need to configure pip as described above.

### New project structure
HEA projects implemented in Python should have the following format:
```
projectname
    README.md
    setup.py
    .gitignore
    src
        module.py
        package/
            __init__.py
            module.py
    tests
        unittestpkg
            unittest.py
```
