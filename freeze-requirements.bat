@echo off

set pytestversion="5.4.2"

set retval=0
set d=%DATE:~-4%-%DATE:~4,2%-%DATE:~7,2%
set t=%TIME::=-%
set tempdirname=%d%T%t%

set usage=Usage: freeze-requirements.bat project-name [project-version]
set helptext=freeze-requirements.bat - Writes out a requirements.txt for a HEA python-based project, and sets the project version in your microservice-versions.env.
set usageoptions=Options:
set usageoptionsprojnm=    project-name: the project folder name.
set usageoptionsprojve=    project-version: the project version number. If omitted, it will use the latest version in the package index.

if "%~1"=="-h" goto HELP
if "%~1"=="--help" goto HELP
if "%~2"=="" goto ERR
goto MAIN

:ERR
echo %usage%
exit /b 1

:HELP
echo %helptext%
echo.
echo %usage%
echo %usageoptions%
echo %usageoptionsprojnm%
echo %usageoptionsprojve%
exit

:MAIN
echo Working...
echo Creating virtual environment in %temp%\venv-%1-%tempdirname%...
python -m venv "%temp%\venv-%1-%tempdirname%"
call "%temp%\venv-%1-%tempdirname%\Scripts\activate"
echo Installing packages...
if "%~2"=="" (pip install "%1" >nul) else (pip install "%1==%2" >nul)
if not "%errorlevel%"=="0" then set retval=1
if "%retval%"=="0" (pip install "pytest==%pytestversion%" >nul) else echo pip install %1 failed && set retval=1 && goto CLEANUP
echo Writing requirements.txt...
if "%retval%"=="0" (pip freeze > "%1\requirements.txt") else echo Installing pytest failed && set retval=1

:UPDATEENV
echo Updating microservice-versions.env file...
>"microservice-versions.env.new" (
    setlocal enabledelayedexpansion
    set _found=0
    for /f "delims=" %%a in (microservice-versions.env) do (
        set _str="%1="
        echo %%a|find !_str! >nul
        if errorlevel 1 (
            setlocal disabledelayedexpansion
            echo %%a
            setlocal enabledelayedexpansion
        ) else (
            set _found=1
            setlocal disabledelayedexpansion
            echo %1=%2
            setlocal enabledelayedexpansion
        )
    )
    if !_found!==0 (echo %1=%2)
    endlocal
)
move /y "microservice-versions.env.new" "microservice-versions.env" >nul

:CLEANUP
call "%temp%\venv-%1-%tempdirname%\Scripts\deactivate"
echo Deleting virtual environment...
del /s /f /q "%temp%\venv-%1-%tempdirname%\*.*" >nul
if not "%errorlevel%"=="0" then set retval=1
for /f %%f in ('dir /ad /b "%temp%\venv-%1-%tempdirname%\"') do rd /s /q "%temp%\venv-%1-%tempdirname%\%%f"
if not "%errorlevel%"=="0" then set retval=1
rmdir "%temp%\venv-%1-%tempdirname%"
if not "%errorlevel%"=="0" then set retval=1
if "%retval%"=="0" (echo Done) else (echo Done with errors)
exit /b %retval%