@echo off
SET workdir=%~dp0

setlocal enabledelayedexpansion
for /f "usebackq tokens=1,* delims==" %%i in ("%workdir%.env") do (
    if "%%i"=="MONGO_HEA_USERNAME" set mongo_hea_username=%%j
    if "%%i"=="MONGO_HEA_PASSWORD" set mongo_hea_password=%%j
)

set cmd=docker exec -it mongo mongo hea -u "%mongo_hea_username%" -p "%mongo_hea_password%" --eval

goto GETOPS

:GETOPS
if "%1"=="-r" goto DROP
if "%1"=="--reset" goto DROP
if "%1"=="-h" goto HELP
if "%1"=="--help" goto HELP
goto CREATE

:DROP
echo Dropping all objects...
%cmd% "db.components.dropIndexes();"
%cmd% "db.components.drop();"
%cmd% "db.folders.dropIndexes();"
%cmd% "db.folders.drop();"
%cmd% "db.folders_items.dropIndexes();"
%cmd% "db.folders_items.drop();"
%cmd% "db.dataadapters.dropIndexes();"
%cmd% "db.dataadapters.drop();"
echo Done!
goto CREATE

:HELP
echo Initializes the HEA database.
exit 0

:CREATE
echo Creating objects...

Rem HEA_REGISTRY

%cmd% "db.components.createIndex({owner: 1, name: 1}, {name: \"idx_components_base_name\", unique: true});"

%cmd% "db.components.insertOne({type: \"heaobject.registry.Component\", created: new ISODate(), name: \"HEA_REGISTRY\", description: \"Registry of HEA services, HEA web clients, and other web sites of interest\", display_name: \"HEA Registry Service\", base_url: \"http://heaserver-registry:8080\", owner: \"system^|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system^|all\", permissions: [\"VIEWER\"]}], resources: [{type: \"heaobject.registry.Resource\", resource_type_name: \"heaobject.registry.Component\", base_path: \"/components\"}, {type: \"heaobject.registry.Resource\", resource_type_name: \"heaobject.registry.Property\", base_path: \"/properties\"}]});"

Rem HEA_FOLDERS

%cmd% "db.folders.createIndex({owner: 1, name: 1}, {name: \"idx_folders_base_name\", unique: true});"
%cmd% "db.folders_items.createIndex({owner: 1, name: 1}, {name: \"idx_folders_children_base_name\", unique: true});"

%cmd% "db.components.insertOne({type: \"heaobject.registry.Component\", created: new ISODate(), name: \"HEA_FOLDERS\", description: \"Repository of folders\", display_name: \"HEA Folder Service\", base_url: \"http://heaserver-folders:8086\", owner: \"system^|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system^|all\", permissions: [\"VIEWER\"]}], resources: [{type: \"heaobject.registry.Resource\", created: new ISODate(), resource_type_name: \"heaobject.folder.Folder\", base_path: \"/folders\"}, {type: \"heaobject.registry.Resource\", created: new ISODate(), resource_type_name: \"heaobject.folder.Item\", base_path: \"/items\"}]});"

Rem HEA_DATA_ADAPTERS

%cmd% "db.dataadapters.createIndex({owner: 1, name: 1}, {name: \"idx_data_adapters_base_name\", unique: true});"

%cmd% "db.components.insertOne({type: \"heaobject.registry.Component\", created: new ISODate(), name: \"HEA_DATA_ADAPTERS\", description: \"Repository of data adapters\", display_name: \"HEA Data Adapter Service\", base_url: \"http://heaserver-data-adapters:8082\", owner: \"system^|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system^|all\", permissions: [\"VIEWER\"]}], resources: [{type: \"heaobject.registry.Resource\", created: new ISODate(), resource_type_name: \"heaobject.dataadapter.DataAdapter\", base_path: \"/dataadapters\"}]});"

Rem SYSTEM PREFERENCES

%cmd% "inserted_doc = db.folders.insertOne({type: \"heaobject.folder.Folder\", name: \"SYSTEM_PREFERENCES\", display_name: \"System Preferences\", description: \"Customize your desktop\", created: ISODate(), owner: \"system^|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system^|all\", permissions: [ \"VIEWER\" ] } ] }); db.folders_items.insertOne({type: \"heaobject.folder.Item\", name: \"SYSTEM_PREFERENCES\", display_name: \"System Preferences\", description: \"Customize your desktop\", created: ISODate(), owner: \"system^|none\", actual_object_type_name: \"heaobject.folder.Folder\", actual_object_id: inserted_doc.insertedId.valueOf(), folder_id: \"root\", shares: [{type: \"heaobject.root.ShareImpl\", created: ISODate(), user: \"system^|all\", permissions: [ \"VIEWER\" ]}]});"

echo Done!
