#!/bin/sh
set -e

cat > /etc/apache2/mods-available/auth_openidc.conf <<EOF
OIDCProviderMetadataURL https://healthenterpriseanalytics.auth0.com/.well-known/openid-configuration
OIDCClientID x0hes8uoiAvooRxSKbkAwXb520OOoT09

OIDCScope "openid name email"
OIDCRedirectURI https://localhost:8443/api/redirect
OIDCCryptoPassphrase '${HEASERVER_API_CRYPTO_PASSPHRASE}'
OIDCRemoteUserClaim sub

OIDCOAuthVerifyJwksUri https://healthenterpriseanalytics.auth0.com/.well-known/jwks.json

<Location /api>
    AuthType oauth20
    Require valid-user
    LogLevel debug
#    Require claim folder:example user.folder = 'example'
</Location>
EOF

exec /usr/sbin/apache2ctl -D FOREGROUND

