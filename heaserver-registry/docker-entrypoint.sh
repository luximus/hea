#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[MongoDB]
ConnectionString=mongodb://${MONGO_HEA_USERNAME}:${MONGO_HEA_PASSWORD}@mongo:27017/${MONGO_HEA_DATABASE:-hea}
EOF

exec heaserver-registry -f .hea-config.cfg -b http://heaserver-registry:8080


