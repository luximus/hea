#!/usr/bin/env bash

WORKDIR=`dirname "$0"`

. $WORKDIR/.env

CMD="docker exec -it mongo mongo ${MONGO_HEA_DATABASE:-hea} --authenticationDatabase hea -u $MONGO_HEA_USERNAME -p $MONGO_HEA_PASSWORD --eval"


# while [ "$1" != "" ]; do
    case $1 in
        -r | --reset )
            echo "Dropping all objects..."
            $CMD "db.components.dropIndexes();"
            $CMD "db.components.drop();"
            $CMD "db.folders.dropIndexes();"
            $CMD "db.folders.drop();"
            $CMD "db.folders_items.dropIndexes();"
            $CMD "db.folders_items.drop();"
            $CMD "db.dataadapters.dropIndexes();"
            $CMD "db.dataadapters.drop();"
            echo "Done!"
        ;;
        -h | --help )
            echo "Initializes the HEA database."
            exit 0
        ;;
        * )
        ;;
    esac
#    shift
#done

echo "Creating objects..."

# HEA_REGISTRY

$CMD "db.components.createIndex({owner: 1, name: 1}, {name: \"idx_components_base_name\", unique: true});"

$CMD "db.components.insertOne({type: \"heaobject.registry.Component\", created: new ISODate(), name: \"HEA_REGISTRY\", description: \"Registry of HEA services, HEA web clients, and other web sites of interest\", display_name: \"HEA Registry Service\", base_url: \"http://heaserver-registry:8080\", owner: \"system|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system|all\", permissions: [\"VIEWER\"]}], resources: [{type: \"heaobject.registry.Resource\", resource_type_name: \"heaobject.registry.Component\", base_path: \"/components\"}, {type: \"heaobject.registry.Resource\", resource_type_name: \"heaobject.registry.Property\", base_path: \"/properties\"}]});"

# HEA_FOLDERS

$CMD "db.folders.createIndex({owner: 1, name: 1}, {name: \"idx_folders_base_name\", unique: true});"
$CMD "db.folders_items.createIndex({owner: 1, name: 1}, {name: \"idx_folders_children_base_name\", unique: true});"

$CMD "db.components.insertOne({type: \"heaobject.registry.Component\", created: new ISODate(), name: \"HEA_FOLDERS\", description: \"Repository of folders\", display_name: \"HEA Folder Service\", base_url: \"http://heaserver-folders:8086\", owner: \"system|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system|all\", permissions: [\"VIEWER\"]}], resources: [{type: \"heaobject.registry.Resource\", created: new ISODate(), resource_type_name: \"heaobject.folder.Folder\", base_path: \"/folders\"}, {type: \"heaobject.registry.Resource\", created: new ISODate(), resource_type_name: \"heaobject.folder.Item\", base_path: \"/items\"}]});"

# HEA_DATA_ADAPTERS

$CMD "db.dataadapters.createIndex({owner: 1, name: 1}, {name: \"idx_data_adapters_base_name\", unique: true});"

$CMD "db.components.insertOne({type: \"heaobject.registry.Component\", created: new ISODate(), name: \"HEA_DATA_ADAPTERS\", description: \"Repository of data adapters\", display_name: \"HEA Data Adapter Service\", base_url: \"http://heaserver-data-adapters:8082\", owner: \"system|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system|all\", permissions: [\"VIEWER\"]}], resources: [{type: \"heaobject.registry.Resource\", created: new ISODate(), resource_type_name: \"heaobject.dataadapter.DataAdapter\", base_path: \"/dataadapters\"}]});"

# SYSTEM PREFERENCES

$CMD "inserted_doc = db.folders.insertOne({type: \"heaobject.folder.Folder\", name: \"SYSTEM_PREFERENCES\", display_name: \"System Preferences\", description: \"Customize your desktop\", created: ISODate(), owner: \"system|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system|all\", permissions: [ \"VIEWER\" ] } ] }); db.folders_items.insertOne({type: \"heaobject.folder.Item\", name: \"SYSTEM_PREFERENCES\", display_name: \"System Preferences\", description: \"Customize your desktop\", created: ISODate(), owner: \"system|none\", actual_object_type_name: \"heaobject.folder.Folder\", actual_object_id: inserted_doc.insertedId.valueOf(), folder_id: \"root\", shares: [{type: \"heaobject.root.ShareImpl\", created: ISODate(), user: \"system|all\", permissions: [ \"VIEWER\" ]}]});"

# Documents

$CMD "inserted_doc = db.folders.insertOne({type: \"heaobject.folder.Folder\", name: \"DOCUMENTS\", display_name: \"Documents\", description: \"Store your documents\", created: ISODate(), owner: \"system|none\", shares: [{type: \"heaobject.root.ShareImpl\", created: new ISODate(), user: \"system|all\", permissions: [ \"VIEWER\" ] } ] }); db.folders_items.insertOne({type: \"heaobject.folder.Item\", name: \"DOCUMENTS\", display_name: \"Documents\", description: \"Store your documents\", created: ISODate(), owner: \"system|none\", type_name: \"heaobject.folder.Folder\", item_id: inserted_doc.insertedId.valueOf(), folder_id: \"root\", shares: [{type: \"heaobject.root.ShareImpl\", created: ISODate(), user: \"system|all\", permissions: [ \"VIEWER\" ]}]});"


echo "Done!"
