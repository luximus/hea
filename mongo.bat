@echo off
SET workdir=%~dp0

setlocal enabledelayedexpansion
for /f "usebackq tokens=1,* delims==" %%i in ("%workdir%.env") do (
    if "%%i"=="MONGO_HEA_USERNAME" set mongo_hea_username=%%j
    if "%%i"=="MONGO_HEA_PASSWORD" set mongo_hea_password=%%j
)

docker exec -it mongo mongo hea -u "%mongo_hea_username%" -p "%mongo_hea_password%"
