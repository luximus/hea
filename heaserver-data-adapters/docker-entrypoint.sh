#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[DEFAULT]
Registry=http://heaserver-registry:8080

[MongoDB]
ConnectionString=mongodb://${MONGO_HEA_USERNAME}:${MONGO_HEA_PASSWORD}@mongo:27017/${MONGO_HEA_DATABASE:-hea}
EOF

exec heaserver-data-adapters -f .hea-config.cfg -b ${HEASERVER_DATA_ADAPTERS_URL}


