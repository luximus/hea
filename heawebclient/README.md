# HEA Web Client
[Research Informatics Shared Resource](https://risr.hci.utah.edu), [Huntsman Cancer Institute](https://hci.utah.edu), 
Salt Lake City, UT

The HEA Web Client is the web browser-based user interface to HEA.

## Version 1
Initial release.

## Runtime requirements
* Major web browsers, including Firefox, Safari, Chrome, Edge, and Internet Explorer version 10 or greater.
* Docker 19.03.5 or greater, available from https://www.docker.com/.

## Build requirements
* Node.js 12.13.1 or greater, available from https://nodejs.org/en/download/ or your package manager.
* Angular CLI 8.3.20 or greater: from your command line, run `npm install -g @angular/cli`. On Mac and Linux, prefix this 
command with `sudo`, like `sudo npm install -g @angular/cli`.
* Any development environment is fine.
* On Windows, you also will need:
    * Build Tools for Visual Studio 2019, found at https://visualstudio.microsoft.com/downloads/. Select the C++ tools.
    * git, found at https://git-scm.com/download/win.
* On Mac, Xcode or the command line developer tools is required, found in the Apple Store app.

## Development notes

A file named `auth_config.json` in the `hea/heawebclient` directory contains configuration for connecting to an
OpenID Connect provider:
```
{
  "clientId": "<the client id from your OpenID Connect identity provider>",
  "domain": "<the domain of your OpenID Connect identity provider>"
}
```
