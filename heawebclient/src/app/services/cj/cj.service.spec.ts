import { TestBed } from '@angular/core/testing';

import { CjService } from './cj.service';

describe('CjService', () => {
  let service: CjService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CjService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
