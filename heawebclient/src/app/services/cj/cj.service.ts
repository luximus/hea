import { Injectable } from '@angular/core';
import {HEAObjectContainer} from '../../models/HEAObjectContainer';
import {HEAObject, Item} from '../../models/HEAObject';


export interface ICJData {
  section: string;
  name: string;
  value: string;
  prompt: string;
  display: string;
}

export interface ICJLink {
  display: string;
  prompt: string;
  rel: string;
  href: string;
}

export interface ICJItem {
  data: ICJData[];
  links: ICJLink[];
}

export interface ICJTemplate {
  data: ICJData[];
}

export interface ICJCollection {
  items: ICJItem[];
  template?: ICJTemplate;
}

export interface ICJObject {
  collection: ICJCollection;
}

@Injectable({
  providedIn: 'root'
})
export class CjService {

  constructor() { }

  asHEAObjectContainers<H extends HEAObject>(cjResults: ICJObject[], heaObjectType: new() => H): HEAObjectContainer<H>[] {
    console.log('converting the following Collection+JSON objects to HEAObjects: ');
    console.log(cjResults);
    const objContainers: HEAObjectContainer<H>[] = [];
    if (cjResults) {
      for (const cjResult of cjResults) {
        if (cjResult && cjResult.collection && cjResult.collection.items) {
          for (const itemArr of cjResult.collection.items) {
            if (itemArr.data) {
              const obj: H = new heaObjectType();
              for (const elt of itemArr.data) {
                if (elt.section) {
                  if (!obj[elt.section]) {
                    obj[elt.section] = {};
                  }
                  obj[elt.section][elt.name] = elt.value;
                } else {
                  obj[elt.name] = elt.value;
                }
              }

              const objContainer = new HEAObjectContainer<H>();
              objContainer.heaObject = obj;
              if (cjResult.collection.template) {
                objContainer.template = [];
              }
              objContainer.links = [];

              if (objContainer.template) {
                for (const d of cjResult.collection.template.data) {
                  objContainer.template.push(d);
                }
              }
              for (const link of itemArr.links) {
                objContainer.links.push(link);
              }
              objContainers.push(objContainer);
            }
          }
        }
      }
    }
    return objContainers;
  }
}
