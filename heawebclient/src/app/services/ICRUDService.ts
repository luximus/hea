import { Observable } from 'rxjs';

export interface ICRUDService<M> {
  delete(obj: M);

  update(obj: M, body: any);

  create(id: string, body: any);

  get(id: string, itemId: string): Observable<M>;

  getAll(id: string): Observable<M[]>;
}
