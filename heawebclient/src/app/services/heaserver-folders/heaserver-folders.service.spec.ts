import { TestBed } from '@angular/core/testing';

import { HEAServerFoldersService } from './heaserver-folders.service';

describe('FileService', () => {
  let service: HEAServerFoldersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HEAServerFoldersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
