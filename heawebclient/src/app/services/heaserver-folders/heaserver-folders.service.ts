import {Injectable} from '@angular/core';
import {HEAObjectContainer} from '../../models/HEAObjectContainer';
import {Item} from '../../models/HEAObject';
import {CjService, ICJObject} from '../cj/cj.service';
import {ApiService} from '../api/api.service';
import {map} from 'rxjs/operators';
import {ICRUDService} from '../ICRUDService';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HEAServerFoldersService implements ICRUDService<HEAObjectContainer<Item>> {

  constructor(private api: ApiService, private cjService: CjService) {
  }

  delete(obj: HEAObjectContainer<Item>) {
    const folderId = obj.heaObject.folder_id;
    console.log(`deleting item ${obj.heaObject.id} in folder ${folderId}`);
    return this.api.delete(`/folders/${folderId}/items/${obj.heaObject.id}`).subscribe(result => {
      console.log(`tried deleting item ${folderId} in folder ${obj.heaObject.id}`);
      console.log(result);
    });
  }

  update(obj: HEAObjectContainer<Item>, template: object) {
    const folderId = obj.heaObject.folder_id;
    console.log(`updating item ${obj.heaObject.id} in folder ${folderId}`);
    return this.api.updateCollectionPlusJson(`/folders/${folderId}/items/${obj.heaObject.id}`, template).subscribe(result => {
      console.log(`tried updating item ${folderId} in folder ${obj.heaObject.id}`);
      console.log(result);
    });
  }

  create(folderId: string, template: object) {
    console.log(`creating item ${template}`);
    return this.api.createCollectionPlusJson(`/folders/${folderId}/items/`, template).subscribe(result => {
      console.log(`tried creating item ${template}`);
      console.log(result);
    });
  }

  clone(obj: HEAObjectContainer<Item>): HEAObjectContainer<Item> {
    return JSON.parse(JSON.stringify(obj));
  }

  get(folderId: string, itemId: string): Observable<HEAObjectContainer<Item>> {
    return this.api.getCollectionPlusJson(`/folders/${folderId}/items/${itemId}`).pipe(map(result => {
      const heaObjectContainers = this.asHEAObjectContainers(result);
      if (heaObjectContainers) {
        return heaObjectContainers[0];
      } else {
        return null;
      }
    }, this));
  }

  getAll(folderId: string): Observable<HEAObjectContainer<Item>[]> {
    return this.api.getCollectionPlusJson(`/folders/${folderId}/items/`).pipe(map(this.asHEAObjectContainers, this));
  }

  private asHEAObjectContainers(items: ICJObject[]) {
    return this.cjService.asHEAObjectContainers(items, Item);
  }
}
