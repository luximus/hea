import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private static ACCEPT_OPTIONS: object = {headers: {Accept: 'application/vnd.collection+json'}};
  private static CONTENT_TYPE_OPTIONS: object = {headers: {'Content-Type': 'application/vnd.collection+json'}};

  private apiUrl = environment.apiURL ? environment.apiURL : '';

  constructor(private http: HttpClient) { }

  ping(): Observable<any> {
    return this.http.get(this.apiUrl + '/folders/root/items' , ApiService.ACCEPT_OPTIONS);
  }

  getCollectionPlusJson(url: string): Observable<any> {
    return this.http.get(this.apiUrl + url, ApiService.ACCEPT_OPTIONS);
  }

  updateCollectionPlusJson(url: string, body: object): Observable<any> {
    return this.http.put(this.apiUrl + url, body, ApiService.CONTENT_TYPE_OPTIONS);
  }

  createCollectionPlusJson(url: string, body: object): Observable<any> {
    return this.http.post(this.apiUrl + url, body, ApiService.CONTENT_TYPE_OPTIONS);
  }

  delete(url: string): Observable<any> {
    return this.http.delete(this.apiUrl + url);
  }

}
