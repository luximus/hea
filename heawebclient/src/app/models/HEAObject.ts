

export class HEAObject {
  id?: string;
  created: string;
  display_name: string;
}

export class Item extends HEAObject {
  actual_object: HEAObject;
  actual_object_type_name: string;
  actual_object_id: string;
  folder_id: string;

  getDisplayName(): string {
    if (this.display_name) {
      return this.display_name;
    } else {
      return this.actual_object.display_name;
    }
  }

  isFolder(): boolean {
    return this.actual_object_type_name === 'heaobject.folder.Folder';
  }
}
