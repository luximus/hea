import {ICJData, ICJLink} from '../services/cj/cj.service';
import {HEAObject} from './HEAObject';

export class HEAObjectContainer<H extends HEAObject> {
  heaObject: H;
  template?: ICJData[];
  links: ICJLink[];
  parent: string;
}
