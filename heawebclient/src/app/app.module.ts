import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeroComponent } from './components/hero/hero.component';
import { LoadingComponent } from './components/loading/loading.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiComponent } from './pages/api/api.component';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatIconModule } from '@angular/material/icon'
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list'
import { MatMenuModule } from '@angular/material/menu'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatDialogModule } from '@angular/material/dialog'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'
import { FormsModule } from '@angular/forms'
import { NewHEAObjectDialogComponent } from './components/heaobject-explorer/modals/new-heaobject-dialog/new-heaobject-dialog.component';
import { RenameHEAObjectDialogComponent } from './components/heaobject-explorer/modals/rename-heaobject-dialog/rename-heaobject-dialog.component';
import {HEAObjectExplorerComponent} from './components/heaobject-explorer/heaobject-explorer.component';
import {HEAServerFoldersService} from './services/heaserver-folders/heaserver-folders.service';
import {HEAObjectPropertiesDialogComponent} from './components/heaobject-properties-dialog/heaobject-properties-dialog.component'
/**
 * Import specific languages to avoid importing everything
 * The following will lazy load highlight.js core script (~9.6KB) + the selected languages bundle (each lang. ~1kb)
 */
export function getHighlightLanguages() {
  return {
    json: () => import('highlight.js/lib/languages/json')
  };
}

@NgModule({
  entryComponents: [
    NewHEAObjectDialogComponent,
    RenameHEAObjectDialogComponent,
    HEAObjectPropertiesDialogComponent
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    NavBarComponent,
    FooterComponent,
    HeroComponent,
    HEAObjectExplorerComponent,
    LoadingComponent,
    ApiComponent,
    NewHEAObjectDialogComponent,
    RenameHEAObjectDialogComponent,
    HEAObjectPropertiesDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    HighlightModule,
    FontAwesomeModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatGridListModule,
    MatMenuModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule
  ],
  providers: [HEAServerFoldersService,
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        languages: getHighlightLanguages()
      }
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
