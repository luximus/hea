import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import {Observable} from 'rxjs';
import {HEAObject} from "../../models/HEAObject";
import {HEAServerFoldersService} from "../../services/heaserver-folders/heaserver-folders.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(private auth: AuthService) {}

  ngOnInit() {
    // On initial load, check authentication state with authorization server
    // Set up local auth streams if user is already authenticated
    this.auth.localAuthSetup();
    // Handle redirect from Auth0 login
    this.auth.handleAuthCallback();
  }


}
