import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import {HEAObjectContainer} from '../../models/HEAObjectContainer';
import {Item} from '../../models/HEAObject';
import {ICJLink} from '../../services/cj/cj.service';
import {NewHEAObjectDialogComponent} from './modals/new-heaobject-dialog/new-heaobject-dialog.component';
import {RenameHEAObjectDialogComponent} from './modals/rename-heaobject-dialog/rename-heaobject-dialog.component';
import {Router} from '@angular/router';
import {HEAServerFoldersService} from '../../services/heaserver-folders/heaserver-folders.service';

@Component({
  selector: 'app-heaobject-explorer',
  templateUrl: './heaobject-explorer.component.html',
  styleUrls: ['./heaobject-explorer.component.css'],
})

export class HEAObjectExplorerComponent implements OnInit {

  @Input() currentRoot: HEAObjectContainer<Item>;

  @Output() folderAdded = new EventEmitter<{ name: string }>();
  @Output() heaObjectRemoved = new EventEmitter<HEAObjectContainer<Item>>();
  @Output() heaObjectRenamed = new EventEmitter<HEAObjectContainer<Item>>();
  @Output() heaObjectMoved = new EventEmitter<{
    heaObject: HEAObjectContainer<Item>
    moveTo: HEAObjectContainer<Item>
  }>();


  @Output() navigatedDown = new EventEmitter<HEAObjectContainer<Item>>();
  @Output() navigatedUp = new EventEmitter();

  path: string;
  heaObjectContainers: HEAObjectContainer<Item>[];
  canNavigateUp: boolean;
  currentPath: string;
  isLoadingData = false;

  constructor(public dialog: MatDialog,
              private foldersService: HEAServerFoldersService,
              public router: Router) {}

  ngOnInit() {
    this.refresh();
  }

  add(objContainer: HEAObjectContainer<Item>) {
    // ToDo
    this.refresh();
  }

  delete(objContainer: HEAObjectContainer<Item>) {
    console.log(`calling delete for item ${objContainer.heaObject.id}`);
    this.foldersService.delete(objContainer);
    this.refresh();
  }

  move(event: { objContainer: HEAObjectContainer<Item>; moveTo: HEAObjectContainer<Item> }) {
    this.foldersService.update(event.objContainer, {parent: event.moveTo.heaObject.id});
    this.refresh();
  }

  rename(objContainer: HEAObjectContainer<Item>) {
    this.foldersService.update(objContainer, {name: objContainer.heaObject.display_name});
    this.refresh();
  }

  navigate(objContainer: HEAObjectContainer<Item>) {
    this.currentRoot = objContainer;
    const openLink  = objContainer.links.find(link => link.prompt.toLowerCase() === 'open');
    const folderId =  openLink.href.substring(openLink.href.lastIndexOf('/') + 1);
    console.log('calling getItemsInFolder for ' + folderId);
    this.foldersService.getAll(folderId);
    this.currentPath = this.pushToPath(this.currentPath, objContainer.heaObject.display_name);
    this.canNavigateUp = true;
  }

  navigateUp() {
    if (this.currentRoot && this.currentRoot.parent === 'root') {
      this.currentRoot = null;
      this.canNavigateUp = false;
      this.refresh();
      this.currentPath = this.popFromPath(this.currentPath);
    } else {
      this.foldersService.get(this.currentRoot.parent, this.currentRoot.heaObject.id).subscribe(result => {
        this.currentRoot = result;
        this.refresh();
        this.currentPath = this.popFromPath(this.currentPath);
      });
    }
  }

  openNewHEAObjectDialog() {
    const dialogRef = this.dialog.open(NewHEAObjectDialogComponent);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.folderAdded.emit({ name: res });
      }
    });
  }

  openRenameHEAObjectDialog(objContainer: HEAObjectContainer<Item>) {
    const dialogRef = this.dialog.open(RenameHEAObjectDialogComponent);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        if (objContainer.heaObject.display_name) {
          objContainer.heaObject.display_name = res;
        } else {
          objContainer.heaObject.actual_object.display_name = res;
        }

        this.heaObjectRenamed.emit(objContainer);
      }
    });
  }

  openMenu(event: MouseEvent, viewChild: MatMenuTrigger) {
    console.log('open menu event');
    console.log(event);
    event.preventDefault();
    viewChild.openMenu();
  }

  performContextualActionFor(objContainer: HEAObjectContainer<Item>, link: ICJLink) {
    if (link.prompt.toLowerCase() === 'Open'.toLowerCase()) {
      console.log('Navigating to element');
      this.navigate(objContainer);
    } else if (link.prompt.toLowerCase() === 'Move'.toLowerCase()) {
      // ToDo
    } else if (link.prompt.toLowerCase() === 'Properties'.toLowerCase()) {
      // ToDo
    }

  }

  refresh() {
    this.isLoadingData = true;
    this.foldersService.getAll(this.currentRoot ? this.currentRoot.heaObject.id : 'root').subscribe(result => {
        this.heaObjectContainers = result;
      },
      error => {
        console.error(`Error getting directory items ${error}`);
      },
      () => {
        this.isLoadingData = false;
        console.log(`Completed refresh`);
      });
  }

  private pushToPath(path: string, folderName: string) {
    let p = path ? path : '';
    p += `${folderName}/`;
    return p;
  }

  private popFromPath(path: string) {
    let p = path ? path : '';
    const split = p.split('/');
    split.splice(split.length - 2, 1);
    p = split.join('/');
    return p;
  }



}
