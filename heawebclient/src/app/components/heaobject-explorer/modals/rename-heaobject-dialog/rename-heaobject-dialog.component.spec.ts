import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RenameHEAObjectDialogComponent } from './rename-heaobject-dialog.component';

describe('RenameHEAObjectDialogComponent', () => {
  let component: RenameHEAObjectDialogComponent;
  let fixture: ComponentFixture<RenameHEAObjectDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RenameHEAObjectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenameHEAObjectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
