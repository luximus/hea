import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewHEAObjectDialogComponent } from './new-heaobject-dialog.component';

describe('NewHEAObjectDialogComponent', () => {
  let component: NewHEAObjectDialogComponent;
  let fixture: ComponentFixture<NewHEAObjectDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHEAObjectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHEAObjectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
