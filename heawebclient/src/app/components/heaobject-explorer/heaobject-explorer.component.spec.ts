import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HEAObjectExplorerComponent } from './heaobject-explorer.component';

describe('HEAObjectExplorerComponent', () => {
  let component: HEAObjectExplorerComponent;
  let fixture: ComponentFixture<HEAObjectExplorerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HEAObjectExplorerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HEAObjectExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
