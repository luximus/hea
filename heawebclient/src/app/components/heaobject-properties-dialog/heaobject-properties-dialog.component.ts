import { Component, OnInit, Input, Output } from '@angular/core';
import {HEAObjectContainer} from '../../models/HEAObjectContainer';
import {HEAObject} from '../../models/HEAObject';

@Component({
  selector: 'app-heaobject-properties-dialog',
  templateUrl: './heaobject-properties-dialog.component.html',
  styleUrls: ['./heaobject-properties-dialog.component.css']
})
export class HEAObjectPropertiesDialogComponent<H extends HEAObject> implements OnInit {

  @Input() inHEAObjectContainer: HEAObjectContainer<H>;

  @Output() outHEAObject: H;

  constructor() { }

  ngOnInit(): void {
  }

}
