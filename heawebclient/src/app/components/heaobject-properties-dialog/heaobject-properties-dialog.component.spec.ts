import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HEAObjectPropertiesDialogComponent } from './heaobject-properties-dialog.component';

describe('NewFolderDialogComponent', () => {
  let component: HEAObjectPropertiesDialogComponent;
  let fixture: ComponentFixture<HEAObjectPropertiesDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HEAObjectPropertiesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HEAObjectPropertiesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
