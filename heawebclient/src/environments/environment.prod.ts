export const environment = {
  production: true,
  apiURL: 'https://localhost:8443/api',
  clientId: 'D7q0XcDjq0qT1L5BaY0OK26ONAqqRgBT',
  domain: 'healthenterpriseanalytics.auth0.com'
};
